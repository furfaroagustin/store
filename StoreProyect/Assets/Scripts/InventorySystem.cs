using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySystem : MonoBehaviour
{
    public KeyCode inventoryKey = KeyCode.I;
    public GameObject inventoryPanel;
    public List<Item> items = new List<Item>();

    public void UpdateInventoryUI(PlayerController playerController)
    {
        for (int i = 0; i < playerController.inventory.Count; i++)
        {
            Image image = inventoryPanel.transform.GetChild(i).GetComponent<Image>();
            image.sprite = playerController.inventory[i].itemImage;
            image.enabled = true;
        }
    }

    void Start()
    {
        inventoryPanel.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(inventoryKey))
        {
            inventoryPanel.SetActive(!inventoryPanel.activeSelf);
        }
    }
}

