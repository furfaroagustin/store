using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 5f;
    private bool canMove = true; // Variable para controlar el movimiento
    public int money;
    public List<Item> inventory = new List<Item>();
    public InventorySystem inventorySystem; // Haz referencia al script del sistema de inventario

 

    void Update()
    {
        if (canMove) // Verifica si el jugador puede moverse
        {
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(horizontalInput, verticalInput, 0) * speed * Time.deltaTime;
            transform.Translate(movement);
        }
    }

    // M�todo para configurar si el jugador puede moverse o no
    public void SetCanMove(bool canMove)
    {
        this.canMove = canMove;
    }


    public void AddItemToInventory(Item item)
    {
        // Busca si ya tienes el art�culo en el inventario
        Item existingItem = inventory.Find(i => i.itemName == item.itemName);

        if (existingItem != null)
        {
            existingItem.quantity++;
        }
        else
        {
            inventory.Add(new Item { itemName = item.itemName, itemImage = item.itemImage, quantity = 1 });
        }
        inventorySystem.UpdateInventoryUI(this);
    }

}
